# LKG codec

An encoding scheme, designed by Liu Kai Ge, a presumably better alternative to UTF-8. Could be made compatible (or not) with UTF-8. Compatible with ASCII.